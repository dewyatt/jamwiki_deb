#!/bin/bash

install --mode=755  --owner tomcat7 --group tomcat7 --directory /srv/jamwiki
chown tomcat7:tomcat7 /var/lib/tomcat7/webapps/jamwiki/WEB-INF/classes

update-rc.d tomcat7 enable
service tomcat7 restart

update-rc.d nginx enable
service nginx restart
