#!/bin/bash
set -eu -o pipefail

~/aptly repo add jamwiki build/distributions/*.deb
~/aptly publish update -architectures="amd64" -skip-signing=true trusty
