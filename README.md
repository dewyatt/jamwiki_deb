# jamwiki_deb

## Description

This creates a debian package from jamwiki.war.

## Usage

1. (Optional) Modify the jamwiki_version file to set the desired jamwiki WAR version to use.
2. Place jamwiki.properties (you must create this from another install) in the jamwiki_deb directory.
3. Run `env BUILD_NUMBER=1 ./build.sh` to manually generate a build.
4. `jamwiki_deb/build/distributions/*.deb` will be generated.

Normally build.sh is called by Jenkins (see the Jenkinsfile), which provides the BUILD_NUMBER environment variable.
