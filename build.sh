#!/bin/bash
set -eu -o pipefail

. jamwiki_version

rm -f jamwiki.war
wget -qO jamwiki.war "http://downloads.sourceforge.net/jamwiki/jamwiki-${jamwiki_version}.war"
./gradlew -Pjamwiki_version=$jamwiki_version -Pbuild_number=$BUILD_NUMBER clean createDeb
